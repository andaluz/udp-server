
//Multicast Client receiving sent messages
var PORT = 41848;
var HOST = '10.100.1.172'; //this is your own IP
//var MCAST_ADDR = "230.185.192.108"; //same mcast address as Server
var MCAST_ADDR = "224.1.1.1";

var dgram = require('dgram');
//var client = dgram.createSocket('udp4');
var client = dgram.createSocket({ type: 'udp4', reuseAddr: true })

client.on('listening', function () {
    var address = client.address();
    console.log('UDP Client listening on ' + address.address + ":" + address.port);
    client.setBroadcast(true)
    client.setMulticastTTL(128);
    client.addMembership(MCAST_ADDR/*, HOST*/);
});

client.on('message', function (message, remote) {
    console.log('MCast Msg: From: ' + remote.address + ':' + remote.port +' - ' + message);
});

client.bind(PORT);//, HOST);
