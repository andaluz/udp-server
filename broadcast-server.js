var PORT = 41848;
//var MCAST_ADDR = "230.185.192.108"; //not your IP and should be a Class D address, see http://www.iana.org/assignments/multicast-addresses/multicast-addresses.xhtml
var HOST = "10.100.1.172";
//var MCAST_ADDR = "230.185.192.108";
var MCAST_ADDR = "224.1.1.1";
var dgram = require('dgram');
var server = dgram.createSocket("udp4");


//Multicast Server sending messages
var news = [
   "Borussia Dortmund wins German championship",
   "Tornado warning for the Bay Area",
   "More rain for the weekend",
   "Android tablets take over the world",
   "iPad2 sold out",
   "Nation's rappers down to last two samples"
];

/*
// emits on new datagram msg
server.on('message',function(msg,info){
  //console.log('Data received from client : ' + msg.toString());
  console.log('Received %d bytes from %s:%d\n',msg.length, info.address, info.port);

  //sending msg
  //server.send(msg, 0, msg.length, PORT,MCAST_ADDR, function(error){
  server.send(msg, info.port, 'localhost', function(error){
    if(error){
      client.close();
    }else{
      console.log('Data sent !!!');
    }
  });

});
*/

server.bind(/*PORT, /*'0.0.0.0',*/ function(){
//server.bind(PORT, '10.100.1.172', function(){
    server.setBroadcast(true);
    server.setMulticastTTL(128);
    server.addMembership(MCAST_ADDR);
    setInterval(broadcastNew, 3000);
});

function broadcastNew() {
    var message = new Buffer(news[Math.floor(Math.random()*news.length)]);
    server.send(message, 0, message.length, PORT, MCAST_ADDR);
    console.log("Sent " + message + " to the wire...");
}
